# zenmonitor3 package for copr
Unofficial [zenmonitor3](https://git.exozy.me/a/zenmonitor3) package built for current supported Fedora Linux and Enterprise Linux releases.

[Copr repo](https://copr.fedorainfracloud.org/coprs/shdwchn10/zenpower3/) | [zenpower3 git repo](https://gitlab.com/shdwchn10/copr-zenpower3) | [zenmonitor3 git repo](https://gitlab.com/shdwchn10/copr-zenmonitor3)

## Installation Instructions

Classic Fedora Linux with dnf, RHEL, EL clones or CentOS Stream:
```
sudo dnf copr enable shdwchn10/zenpower3

sudo dnf install zenpower3 zenmonitor3

systemctl reboot
```

Fedora Atomic Desktops/IoT/CoreOS 41+:
```
sudo dnf copr enable shdwchn10/zenpower3

rpm-ostree install zenpower3 zenmonitor3

systemctl reboot
```

Fedora Atomic Desktops/IoT/CoreOS 40 or older:
```
sudo wget -O /etc/yum.repos.d/_copr:copr.fedorainfracloud.org:shdwchn10:zenpower3.repo https://copr.fedorainfracloud.org/coprs/shdwchn10/zenpower3/repo/fedora-$(rpm -E %fedora)/shdwchn10-zenpower3-fedora-$(rpm -E %fedora).repo

rpm-ostree install zenpower3 zenmonitor3

systemctl reboot
```
